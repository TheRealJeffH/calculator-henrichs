package edu.towson.cosc431.henrichs.calculator

import android.annotation.SuppressLint
import android.content.ComponentName
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var num1: Int? = null
    var num2: Int = 0
    var f_num1: Float? = null
    var f_num2: Float = 0.0f
    var value: Int? = null
    var input: String = ""
    var isFloat: Boolean = false
    var isNum1Float: Boolean = false
    var isNum2Float: Boolean = false
    var total: Int? = null
    var f_total: Float? = null
    var addOp: Boolean = false
    var subOp: Boolean = false
    var multOp: Boolean = false
    var divOp: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        add.setOnClickListener { performAddition() }
        sub.setOnClickListener { performSubtraction() }
        mult.setOnClickListener { performMultiplication() }
        div.setOnClickListener { performDivision() }
        equals.setOnClickListener { performMath() }

        gridView.adapter = ImageAdapter(this)

        gridView.onItemClickListener = AdapterView.OnItemClickListener{
            parent, v, position, id ->

            textView.setText(" ")
            
            if (position > -1 && position < 9){


                value = position + 1
                input += value.toString()
                Toast.makeText(this, "$value", Toast.LENGTH_LONG).show()
                textView.append(input)
            } else if(position == 9){

                Toast.makeText(this, ".", Toast.LENGTH_LONG).show()
                input += "."
                textView.append(".")
                isFloat = true

            } else if(position == 10){

                value = 0
                Toast.makeText(this, "$value", Toast.LENGTH_LONG).show()
                input += value.toString()
                textView.append(input)

            } else if(position == 11){

                Toast.makeText(this, "CLEAR", Toast.LENGTH_LONG).show()
                input = ""
                textView.setText(input)
            }

        }
    }


    fun performAddition(){

        if(isFloat){
            f_num1 = input?.toFloat()
            isNum1Float = true
            isFloat = false
        }else{
            num1 = input?.toInt()

        }
        input = ""
        textView.setText("Enter a second number")
        Toast.makeText(this, "+", Toast.LENGTH_LONG).show()
        addOp = true
    }

    fun performSubtraction(){

        if(isFloat){
            f_num1 = input?.toFloat()
            isNum1Float = true
            isFloat = false
        }else
            num1 = input?.toInt()

        input = ""
        textView.setText("Enter a second number")
        Toast.makeText(this, "-", Toast.LENGTH_LONG).show()
        subOp = true
    }

    fun performMultiplication(){

        if(isFloat){
            f_num1 = input.toFloat()
            isNum1Float = true
            isFloat = false
        }else
            num1 = input.toInt()

        input = ""
        textView.setText("Enter a second number")
        Toast.makeText(this, "x", Toast.LENGTH_LONG).show()
        multOp = true
    }

    fun performDivision(){

        if(isFloat){
            f_num1 = input.toFloat()
            isNum1Float = true
            isFloat = false
        }else
            num1 = input.toInt()

        input = ""
        textView.setText("Enter a second number")
        Toast.makeText(this, "/", Toast.LENGTH_LONG).show()
        divOp = true
    }

    @SuppressLint("SetTextI18n")
    fun performMath(){

        if(isFloat){
            f_num2 = input.toFloat()
            isNum2Float = true
            isFloat = false
        }else
            num2 = input.toInt()


        if(subOp){
            if(!isNum1Float && !isNum2Float){
                total = num1?.minus(num2)
                textView.setText("$total")
            }else if(!isNum1Float && isNum2Float){
                f_total = num1?.toFloat()?.minus(f_num2)
                textView.setText("$f_total")
            }else if(isNum1Float && !isNum2Float){
                f_total = f_num1?.minus(num2.toFloat())
                textView.setText("$f_total")
            }else if(isNum1Float && isNum2Float){
                f_total = f_num1?.minus(f_num2)
                textView.setText("$f_total")
            }
        }else if(addOp){
            if(!isNum1Float && !isNum2Float){
                total = num1?.plus(num2)
                textView.setText("$total")
            }else if(!isNum1Float && isNum2Float){
                f_total = num1?.toFloat()?.plus(f_num2)
                textView.setText("$f_total")
            }else if(isNum1Float && !isNum2Float){
                f_total = f_num1?.plus(num2.toFloat())
                textView.setText("$f_total")
            }else if(isNum1Float && isNum2Float){
                f_total = f_num1?.plus(f_num2)
                textView.setText("$f_total")
            }
        }else if(multOp){
            if(!isNum1Float && !isNum2Float){
                total = num1?.times(num2)
                textView.setText("$total")
            }else if(!isNum1Float && isNum2Float){
                f_total = num1?.toFloat()?.times(f_num2)
                textView.setText("$f_total")
            }else if(isNum1Float && !isNum2Float){
                f_total = f_num1?.times(num2.toFloat())
                textView.setText("$f_total")
            }else if(isNum1Float && isNum2Float){
                f_total = f_num1?.times(f_num2)
                textView.setText("$f_total")
            }
        }else if(divOp){

            if(!isNum1Float && !isNum2Float){
                if(num2.equals(0)){
                    textView.setText("ERROR - Cannot divide by zero")
                }else{
                    total = num1?.div(num2)
                    textView.setText("$total")
                }
            }else if(!isNum1Float && isNum2Float){
                if(f_num2.equals(0.0f)){
                    textView.setText("ERROR - Cannot divide by zero")
                }else{
                    f_total = num1?.toFloat()?.div(f_num2)
                    textView.setText("$f_total")
                }
            }else if(isNum1Float && !isNum2Float){
                if(num2.equals(0)){
                    textView.setText("ERROR - Cannot divide by zero")
                }else{
                    f_total = f_num1?.div(num2.toFloat())
                    textView.setText("$f_total")
                }
            }else if(isNum1Float && isNum2Float){
                if(f_num2.equals(0.0f)){
                    textView.setText("ERROR - Cannot divide by zero")
                }else {
                    f_total = f_num1?.div(f_num2)
                    textView.setText("$f_total")
                }
            }

        }


        total = null
        f_total = 0.0f
        isNum2Float = false
        isNum1Float = false
        addOp = false
        subOp = false
        multOp = false
        divOp = false
        input = ""
        Toast.makeText(this, "=", Toast.LENGTH_LONG).show()
    }

}
