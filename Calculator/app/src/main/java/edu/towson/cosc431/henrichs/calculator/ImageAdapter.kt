package edu.towson.cosc431.henrichs.calculator

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView


val mThumbIds = arrayOf<Int>(
        R.drawable.sample_1, R.drawable.sample_2, R.drawable.sample_3,
        R.drawable.sample_4, R.drawable.sample_5, R.drawable.sample_6,
        R.drawable.sample_7, R.drawable.sample_8, R.drawable.sample_9,
        R.drawable.peroid, R.drawable.sample_0, R.drawable.clear)

class ImageAdapter(val mContext: Context) : BaseAdapter() {

    override fun getCount(): Int = mThumbIds.size

    override fun getItem(position: Int): Any? = null

    override fun getItemId(position: Int): Long = 0L

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val imageView: ImageView
        if(convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = ImageView(mContext)
            imageView.layoutParams = ViewGroup.LayoutParams(250, 250)
            imageView.scaleType = ImageView.ScaleType.CENTER_CROP
            imageView.setPadding(8 , 8, 8, 8)
        } else {
            imageView = convertView as ImageView
        }

        imageView.setImageResource(mThumbIds[position])
        return imageView
    }

    

}